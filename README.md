Automatization of a wordpress site using Ansible and Vagrant with the default centos/7 box
by Juan Pablo Yanez Camacho, 22/03/2018



Execution instructions
=========
            
1- Connect to your local machine and make sure you have vagrant and Git installed

2- Clone the repository into your machine using the command that will appear when you click in the CLONE button on the SOURCE section of this 
repository, it should look like this: 
                                    
                                        git clone https://username@bitbucket.org/bassjompi/juan-pablo.git

3- go to the cloned repository    

                                        cd ./juan-pablo

4- once you are in the directory where the repo was cloned just issue the the following command to download (if not present), creates the vm and provisions it with Ansible:  
        
                                        vagrant up
        
                                        
you will be prompted eventually for the Ansible vault password, the password is  Welcome!

5- You can check if it worked trying to access to the Wordpress page we just created, from your internet browser try to get to
                                        
                                        http://192.168.33.13/
                                        
                                        
            
            
            
Description of the repository
=========
		

playbook.yml
----------------

This is the main playbook for the ansible job that i have created in yaml

hosts: all   -----> Specifies the target machine, specifying all with no hosts file will use localhost as default.
Roles        -----> The different tasks has been separated into different roles for scalability reasons. Check the role description for more info
	


Roles
----------------

The tasks has been divided into different roles, each of them in a separate folder within the /roles  folder (they have been initialized with ansible galaxy)

Server -----> This role has been designed to install all the components needed to run httpd 
	          please check  the file in /roles/server/tasks/main.yml
	     
     
      -name: install the latest version of Apache httpd
      yum:     ## uses the function yum in order to install new packages in centos
        name: httpd     ## name of the package to install
        state: latest   ## pick the latest version of the package
        become: true     ## become sudo user


      -name: start the httpd service    ## start the httpd server
      service: 
          name=httpd 
          state=started
      become: true

      -name: Open port 80        ## opening the port 80 where the apache server will be working
      firewalld: service=http permanent=true state=enabled
      become: true

      -name: start the firewalld service     # restart the firewall service so it will apply the changes         
      service: name=firewalld state=restarted
      become: true
---  
  
php ------> This role has been designed to install all the components to run php that are php, php-gd and php-mysql
            please check the file in /roles/php/tasks/main.yml   all the tasks are using the yum function as in the server role


mariadb  -------> This role has been designed to install all the components to run a mysql database using mariadb. It will also
                  create a new database called wordpress and a new user/password for this database and then grant all the privileges.
                  The file with the details can be found in  /roles/mariadb/defaults/main.yml but this file has been encrypted using 
                  ansible-vault. The password needed for decryption can be found in the EXECUTION INSTRUCTIONS part on this readme file
                  The main yaml file can be found at roles/mariadb/tasks/main.yml
                  

    -name: install the latest version of MariaDb server  ##install package mariaDb server (yum)
    yum:
      name: mariadb-server
      state: latest
    become: true

    -name: install the latest version of MariaDb    ##install mariadb
    yum:
      name: mariadb
      state: latest
    become: true

    -name: install python-msql   ##install MySQL-python package, needed
    yum:
      name: MySQL-python
      state: latest
    become: true

    -name: start the MariaDB server    ## start of the mariadb server so we can get into the mysql console
    service: name=mariadb state=restarted
    become: true

    -name: Create mysql database     ## login into mysql as root user and then creates a database (variable retrieved from /roles/mariadb/defaults/main.yml)
    mysql_db: name={{ wp_mysql_db }} state=present login_user=root login_password=""

    -name: Create mysql user  ##creates the wordpress user and grants all the permissions (variable retrieved from /roles/mariadb/defaults/main.yml)
    mysql_user: 
      name={{ wp_mysql_user }} 
      password={{ wp_mysql_password }} 
      priv=*.*:ALL
      login_user=root 
      login_password=""
---

wordpress  -----> This role has been designed to install all the components to run wordpress and configure it to run. This time we will 
                  download a zipped file so we will first download it and then unzip it. The main yaml file can be found at roles/wordpress/tasks/main.yml

---

    -name: Download WordPress      #### download the compressed file into tmp folder
    get_url: 
      url: https://wordpress.org/latest.tar.gz 
      dest: /tmp/wordpress.tar.gz

    -name: Extract WordPress  ### Extracts the file into target folder /var/www
    unarchive:
      src=/tmp/wordpress.tar.gz 
      dest=/var/www/ 
      copy=no 
    become: true

    -name: Update default Apache site  #### update Apache's default site document root to point to our site:
    lineinfile: 
      dest=/etc/httpd/conf/httpd.conf 
      regexp="(.)+DocumentRoot /var/www/html"
      line="DocumentRoot /var/www/wordpress"
    notify:
      -restart apache      ## This handler will be called whe restart apache is changed, causing the server to restart Apache.
                          ## this handler can be found in roles/wordpress/handlers/main.yml
    become: true

    -name: Copy sample config file
    command: mv /var/www/wordpress/wp-config-sample.php /var/www/wordpress/wp-config.php creates=/var/www/wordpress/wp-config.php
    become: true

    -name: Update WordPress config file
    lineinfile:
      dest=/var/www/wordpress/wp-config.php   ### update of the constants so it will match with our database/user/password
      regexp="{{ item.regexp }}"
      line="{{ item.line }}"
    with_items:
      -{'regexp': "define\\('DB_NAME', '(.)+'\\);", 'line': "define('DB_NAME', '{{wp_mysql_db}}');"}        
      -{'regexp': "define\\('DB_USER', '(.)+'\\);", 'line': "define('DB_USER', '{{wp_mysql_user}}');"}        
      -{'regexp': "define\\('DB_PASSWORD', '(.)+'\\);", 'line': "define('DB_PASSWORD', '{{wp_mysql_password}}');"}
    become: true

    

Vagrantfile
----------------

  This is the main vagrant configuration file that we will use to spin our Virtual machine and provision it with
                     the ansible tasks that we have created. The lines that have been uncommented/changed/added are:
                     
                     -config.vm.box = "centos/7"     ##specifies the box that we want to download/run in this case centos 7 from official repo

                     -config.vm.network "forwarded_port", guest: 80, host: 8081  ##Create a forwarded port mapping which allows access to a specific port
  
                     -config.vm.network "private_network", ip: "192.168.33.13" ## Specifying the IP that will be used in the private network
                     
                     -config.vm.provision :ansible do |ansible|  ## forces the provisioning of the vm using ansible
                     -ansible.playbook = "playbook.yml"  ## specifies the playbook to be used 
                     -ansible.ask_vault_pass = true  ## forces the prompt of password for ansible-vault encrypted files



